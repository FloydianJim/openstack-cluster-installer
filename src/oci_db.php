<?php
// Automatic database array generation for OCI
// Generation date: 2019-03(Mar)-14 Thursday 14:42
$database = array(
"version" => "1.0.0",
"tables" => array(
	"blockdevices" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"machine_id" => "int(11) NOT NULL default '0'",
			"name" => "varchar(255) NOT NULL default ''",
			"realdev" => "varchar(32) NOT NULL default ''",
			"size_mb" => "int(11) NOT NULL default '0'"
			),
		"primary" => "(id)"
		),
	"blkdev_ctrl" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"machine_id" => "int(11) NOT NULL default '0'",
			"hwid" => "varchar(64) NOT NULL default 'none'",
			"vendor" => "varchar(64) NOT NULL default 'none'",
			"product" => "varchar(64) NOT NULL default 'none'",
			"ctrl_type" => "varchar(64) NOT NULL default 'none'",
			"firmware_version" => "varchar(64) NOT NULL default '0.0.0'",
			),
		"primary" => "(id)",
		),
	"physblockdevices" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"machine_id" => "int(11) NOT NULL default '0'",
			"controller" => "int(11) NOT NULL default '0'",
			"enclosure" => "int(11) NOT NULL default '0'",
			"slot" => "int(11) NOT NULL default '0'",
			"model" => "varchar(128) NOT NULL default ''",
			"serial" => "varchar(128) NOT NULL default ''",
			"size" => "bigint(11) NOT NULL default '0'",
			"state" => "varchar(128) NOT NULL default 'no-state'",
			),
		"primary" => "(id)"
		),
	"clusters" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(64) NOT NULL default ''",
			"domain" => "varchar(253) NOT NULL default ''",
			"region_name" => "varchar(32) NOT NULL default 'RegionOne'",
			"initial_cluster_setup" => "enum('yes','no') NOT NULL default 'yes'",
			"initial_mon_on_ctrl" => "enum('yes','no') NOT NULL default 'yes'",
			"vip_hostname" => "varchar(255) NOT NULL default ''",
			"first_master_machine_id" => "int(11) NULL default NULL",
			"first_sql_machine_id" => "int(11) NULL default NULL",
			"first_sqlmsg_machine_id" => "int(11) NULL default NULL",
			"first_rabbit_machine_id" => "int(11) NULL default NULL",
			"swift_part_power" => "int(11) NOT NULL default '19'",
			"swift_replicas" => "int(11) NOT NULL default '3'",
			"swift_min_part_hours" => "int(11) NOT NULL default '1'",
			"swift_proxy_hostname" => "varchar(255) NOT NULL default ''",
			"swift_encryption_key_id" => "varchar(128) NOT NULL default ''",
			"swift_disable_encryption" => "enum('yes','no') NOT NULL default 'yes'",
			"swift_object_replicator_concurrency" => "int(11) NOT NULL default '5'",
			"swift_rsync_connection_limit" => "int(11) NOT NULL default '16'",
			"swift_public_cloud_middlewares" => "enum('yes','no') NOT NULL default 'no'",
			"extswift_use_external" => "enum('yes','no') NOT NULL default 'no'",
			"extswift_auth_url" => "varchar(255) NOT NULL default 'https://api.example.com/identity'",
			"extswift_region" => "varchar(255) NOT NULL default 'RegionOne'",
			"extswift_proxy_url" => "varchar(255) NOT NULL default 'https://prx.example.com/object/v1/AUTH_'",
			"extswift_project_name" => "varchar(128) NOT NULL default ''",
			"extswift_project_domain_name" => "varchar(128) NOT NULL default 'default'",
			"extswift_user_name" => "varchar(128) NOT NULL default ''",
			"extswift_user_domain_name" => "varchar(128) NOT NULL default 'default'",
			"extswift_password" => "varchar(128) NOT NULL default ''",
			"haproxy_custom_url" => "varchar(255) NOT NULL default ''",
			"statsd_hostname" => "varchar(255) NOT NULL default ''",
			"time_server_host" => "varchar(255) NOT NULL default '0.debian.pool.ntp.org'",
			"amp_secgroup_list" => "varchar(255) NOT NULL default ''",
			"amp_boot_network_list" => "varchar(255) NOT NULL default ''",
			"disable_notifications" => "enum('yes','no') NOT NULL default 'no'",
			"enable_monitoring_graphs" => "enum('yes','no') NOT NULL default 'no'",
			"monitoring_graphite_host" => "varchar(255) NOT NULL default ''",
			"monitoring_graphite_port" => "int(6) NOT NULL default '2003'",
			"self_signed_api_cert" => "enum('yes','no') NOT NULL default 'yes'",
			"nested_virt" => "enum('yes','no') NOT NULL default 'no'",
			"use_ovs_ifaces" => "enum('yes','no') NOT NULL default 'no'",
			"cpu_mode" => "enum('host-model','host-passthrough','custom') NOT NULL default 'host-passthrough'",
			"cpu_model" => "varchar(128) NOT NULL default ''",
			"cpu_model_extra_flags" => "varchar(128) NOT NULL default ''",
			"hw_machine_type" => "varchar(128) NOT NULL default 'q35'",
			"nova_skip_cpu_compare_on_dest" => "enum('yes','no') NOT NULL default 'no'",
			"nova_skip_cpu_compare_at_startup" => "enum('yes','no') NOT NULL default 'no'",
			"nova_skip_hypervisor_version_check_on_lm" => "enum('yes','no') NOT NULL default 'no'",
			"nameserver_v4_prim" => "varchar(32) NOT NULL default '9.9.9.9'",
			"nameserver_v4_sec" => "varchar(32) NOT NULL default '8.8.8.8'",
			"nameserver_v6_prim" => "varchar(128) NOT NULL default '2620:fe::fe:9'",
			"nameserver_v6_sec" => "varchar(128) NOT NULL default '2001:4860:4860::8888'",
			"cephosd_automatic_provisionning" => "enum('yes','no') NOT NULL default 'yes'",
			"neutron_dnsmasq_dns_servers" => "varchar(255) NOT NULL default 'none'",
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(name)"
			)
		),
	"ifnames" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"machine_id" => "int(11) NOT NULL default '0'",
			"name" => "varchar(255) NOT NULL default ''",
			"macaddr" => "varchar(20) NOT NULL default ''",
			"max_speed" => "int(11) NOT NULL default '10'",
			"switchport_name" => "varchar(255) NOT NULL default 'unknown'",
			"switch_hostname" => "varchar(255) NOT NULL default 'unknown'",
			"firmware_version" => "varchar(32) NOT NULL default '0.0.0'",
			"driver" => "varchar(32) NOT NULL default 'none'",
			),
		"primary" => "(id)"
		),
	"ips" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"network" => "int(11) NOT NULL default '0'",
			"ip" => "bigint(128) NOT NULL default '0'",
			"type" => "enum('4','6') NOT NULL default '4'",
			"machine" => "int(11) NOT NULL default '0'",
			"usefor" => "enum('machine','vip','ipmi') NOT NULL default 'machine'",
			"vip_usage" => "varchar(64) NOT NULL default 'api'"
			),
		"primary" => "(id)",
		"keys" => array(
			"uniqueip" => "(ip)",
			"uniquemachine" => "(network,machine,usefor,vip_usage)"
			)
		),
	"locations" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(64) NOT NULL default ''",
			"swiftregion" => "varchar(64) NOT NULL default ''"
			),
		"primary" => "(id)",
		"keys" => array(
			"uniquename" => "(name)"
			)
		),
	"machines" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"memory" => "int(11) NOT NULL default '0'",
			"dhcp_ip" => "varchar(32) NOT NULL default '0.0.0.0'",
			"ipaddr" => "varchar(32) NOT NULL default ''",
			"serial" => "varchar(128) NOT NULL default ''",
			"system_manufacturer" => "varchar(128) NOT NULL default ''",
			"product_name" => "varchar(128) NOT NULL default ''",
			"hostname" => "varchar(255) NOT NULL default ''",
			"installed" => "enum('yes','no') NOT NULL default 'no'",
			"puppet_status" => "enum('notrun','running','success','failure') NOT NULL default 'notrun'",
			"lastseen" => "timestamp NULL default NULL",
			"report_counter" => "int(11) NOT NULL default '0'",
			"status" => "varchar(128) NOT NULL default 'None'",
			"role" => "varchar(64) NOT NULL default ''",
			"cluster" => "int(11) NULL default NULL",
			"ipmi_use" => "enum('yes','no') NOT NULL default 'no'",
			"ipmi_call_chassis_bootdev" => "enum('yes','no') NOT NULL default 'no'",
			"ipmi_addr" => "varchar(32) NOT NULL default ''",
			"ipmi_port" => "int(11) NOT NULL default '623'",
			"ipmi_vlan" => "varchar(32) NOT NULL default 'off'",
			"ipmi_username" => "varchar(64) NOT NULL default ''",
			"ipmi_password" => "varchar(64) NOT NULL default ''",
			"ipmi_default_gw" => "varchar(32) NOT NULL default ''",
			"ipmi_netmask" => "varchar(32) NOT NULL default '255.255.255.0'",
			"ipmi_set_in_progress" => "enum('yes','no') NOT NULL default 'no'",
			"location_id" => "int(11) NULL default NULL",
			"notes" => "varchar(256) NOT NULL default ''",
			"loc_dc" => "varchar(32) NOT NULL default ''",
			"loc_row" => "varchar(32) NOT NULL default ''",
			"loc_rack" => "varchar(32) NOT NULL default ''",
			"loc_u_start" => "varchar(16) NOT NULL default ''",
			"loc_u_end" => "varchar(16) NOT NULL default ''",
			"ladvd_report" => "varchar(128) NOT NULL default ''",
			"bios_version" => "varchar(64) NOT NULL default ''",
			"dell_lifecycle_version" => "varchar(32) NOT NULL default ''",
			"ipmi_firmware_version" => "varchar(32) NOT NULL default ''",
			"ipmi_detected_ip" => "varchar(64) NOT NULL default ''",
			"use_ceph_if_available" => "enum('yes','no') NOT NULL default 'no'",
			"dest_blk" => "varchar(32) NOT NULL default 'none'",
			"install_on_raid" => "enum('yes','no') NOT NULL default 'no'",
			"raid_type" => "enum('0','1','10','5') NOT NULL default '1'",
			"raid_dev0" => "varchar(64) NOT NULL default 'sda'",
			"raid_dev1" => "varchar(64) NOT NULL default 'sdb'",
			"raid_dev2" => "varchar(64) NOT NULL default 'sdc'",
			"raid_dev3" => "varchar(64) NOT NULL default 'sdd'",
			"serial_console_dev" => "varchar(16) NOT NULL default 'ttyS1'",
			"use_gpu" => "enum('yes','no') NOT NULL default 'no'",
			"gpu_name" => "varchar(64) NOT NULL default 'c5p1000'",
			"gpu_vendor_id" => "varchar(16) NOT NULL default '10de'",
			"gpu_product_id" => "varchar(16) NOT NULL default '1cb1'",
			"gpu_device_type" => "varchar(16) NOT NULL default 'type-PCI'",
			"vfio_ids" => "varchar(64) NOT NULL default ''",
			"nested_virt" => "enum('cluster_value','yes','no') NOT NULL default 'cluster_value'",
			"cpu_mode" => "enum('cluster_value','host-model','host-passthrough','custom') NOT NULL default 'cluster_value'",
			"cpu_model" => "varchar(255) NOT NULL default 'cluster_value'",
			"cpu_model_extra_flags" => "varchar(255) NOT NULL default 'cluster_value'",
			"hw_machine_type" => "varchar(255) NOT NULL default 'cluster_value'",
			"nova_skip_cpu_compare_on_dest" => "enum('cluster_value','yes','no') NOT NULL default 'cluster_value'",
			"nova_skip_cpu_compare_at_startup" => "enum('cluster_value','yes','no') NOT NULL default 'cluster_value'",
			"nova_skip_hypervisor_version_check_on_lm" => "enum('cluster_value','yes','no') NOT NULL default 'cluster_value'",
			"force_dhcp_agent" => "enum('yes','no') NOT NULL default 'no'",
			"swift_store_account" => "enum('yes','no') NOT NULL default 'yes'",
			"swift_store_container" => "enum('yes','no') NOT NULL default 'yes'",
			"swift_store_object" => "enum('yes','no') NOT NULL default 'yes'",
			"ceph_osd_initial_setup" => "enum('yes','no') NOT NULL default 'yes'",
			"force_no_bgp2host" => "enum('yes','no') NOT NULL default 'no'",
			"force_frr" => "enum('yes','no') NOT NULL default 'no'",
			"force_frr_nic1" => "varchar(16) NOT NULL default 'ens4'",
			"force_frr_nic2" => "varchar(16) NOT NULL default 'ens5'",
			"force_frr_vlan" => "int(11) NOT NULL default '10'",
			"use_oci_sort_dev" => "enum('yes','no') NOT NULL default 'no'",
			"cpu_vendor" => "varchar(32) NOT NULL default 'none'",
			"cpu_model_name" => "varchar(64) NOT NULL default 'none'",
			"cpu_mhz" => "varchar(16) NOT NULL default '0'",
			"cpu_threads" => "int(11) NOT NULL default '0'",
			"cpu_core_per_socket" => "int(11) NOT NULL default '0'",
			"cpu_sockets" => "int(11) NOT NULL default '0'",
			"boot_uefi" => "enum('yes','no') NOT NULL default 'no'",
			"secure_boot" => "enum('yes','no') NOT NULL default 'no'",
			"ceph_az" => "int(11) NOT NULL default '1'",
			),
		"primary" => "(id)",
		"keys" => array(
			"serial" => "(serial)"
			)
		),
	"networks" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(128) NOT NULL default ''",
			"ip" => "varchar(64) NOT NULL default ''",
			"cidr" => "int(3) NOT NULL default '24'",
			"first_ip" => "bigint(128) NOT NULL default '0'",
			"last_ip" => "bigint(128) NOT NULL default '0'",
			"is_public" => "enum('yes','no') NOT NULL default 'no'",
			"cluster" => "int(11) NULL default NULL",
			"role" => "varchar(64) NULL default NULL",
			"vip_usage" => "enum('controller','messaging','sql','sqlmsg') NOT NULL default 'controller'",
			"iface1" => "varchar(32) NULL default NULL",
			"iface2" => "varchar(32) NULL default NULL",
			"lldp_match" => "varchar(64) NOT NULL default 'None'",
			"bridgename" => "varchar(32) NULL default NULL",
			"vlan" => "int(11) NULL default NULL",
			"mtu" => "int(11) NOT NULL default '0'",
			"location_id" => "int(11) NULL default NULL",
			"ipmi_match_addr" => "varchar(64) NOT NULL default '0.0.0.0'",
			"ipmi_match_cidr" => "int(3) NOT NULL default '0'",
			"ipmi_mode" => "enum('ocimanaged','externaldhcp') NOT NULL default 'ocimanaged'",
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(name)"
			)
		),
	"passwords" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"cluster" => "int(11) NOT NULL default '0'",
			"service" => "varchar(64) NOT NULL default ''",
			"passtype" => "varchar(64) NOT NULL default ''",
			"pass" => "varchar(128) NOT NULL default ''",
			"passtxt1" => "text character set utf8 collate utf8_unicode_ci",
			"passtxt2" => "text character set utf8 collate utf8_unicode_ci",
			"az" => "int(11) NOT NULL default '0'",
			),
		"primary" => "(id)"
		),
	"sshkeypairs" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"clusterid" => "int(11) NOT NULL default '0'",
			"hostname" => "varchar(255) NOT NULL default ''",
			"username" => "varchar(64) NOT NULL default ''",
			"pubkey" => "text character set utf8 collate utf8_unicode_ci",
			"privatekey" => "text character set utf8 collate utf8_unicode_ci"
			),
		"keys" => array(
			"uniquekeypair" => "(clusterid,hostname,username)",
                        ),
		"primary" => "(id)"
		),
	"rolecounts" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"cluster" => "int(11) NOT NULL default '0'",
			"role" => "int(11) NOT NULL default '0'",
			"count" => "int(11) NOT NULL default '0'"
			),
		"primary" => "(id)",
		"keys" => array(
			"cluster" => "(cluster,role)"
			)
		),
	"roles" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(64) NOT NULL default ''"
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(name)"
			)
		),
	"swiftregions" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(64) NOT NULL default ''"
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(name)"
			)
		),
	"swiftrestrictions" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"name" => "varchar(64) NOT NULL default ''",
			"project_id" => "varchar(64) NOT NULL default ''",
			"ip_address" => "varchar(32) NOT NULL default '192.168.0.0'",
			"ip_cidr" => "int(3) NOT NULL default '24'",
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(name)"
			)
		),
	"users" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"login" => "varchar(128) NOT NULL default ''",
			"hashed_password" => "varchar(128) NOT NULL default ''",
			"use_radius" => "enum('yes','no') NOT NULL default 'yes'",
			"activated" => "enum('yes','no') NOT NULL default 'yes'",
			"is_admin" => "enum('yes','no') NOT NULL default 'no'"
			),
		"primary" => "(id)",
		"keys" => array(
			"name" => "(login)"
			)
		),
	"filebeathosts" => array(
		"vars" => array(
			"id" => "int(11) NOT NULL auto_increment",
			"cluster" => "int(11) NULL default NULL",
			"hostname" => "varchar(128) NOT NULL default ''",
			"port" => "int(6) NOT NULL default '5044'",
			),
		"primary" => "(id)",
		),
	)
);
?>
