class oci::configure_backports(
  $ceph_configure_osbpo       = false,
  $nova_use_qemu_backports    = false,
  $nova_use_libvirt_backports = false,
){

  if $::lsbdistcodename == undef {
    # This works around differences between facter versions
    if $facts['os']['lsb'] != undef {
      $lsbdist = $facts['os']['lsb']['distcodename']
    }else{
      $lsbdist = $facts['os']['distro']['codename']
    }
  }else{
    $lsbdist = downcase($::lsbdistcodename)
  }

  if $lsbdist == "bookworm" {
    if $ceph_configure_osbpo or $nova_use_qemu_backports or $nova_use_libvirt_backports {
      # Always set priority to the osbpo repo.
      # Later, we set or not the *.source files.
      file { '/etc/apt/preferences.d/99-prio-from-osbpo':
        ensure                  => present,
        owner                   => 'root',
        group                   => 'root',
        mode                    => '0644',
        selinux_ignore_defaults => true,
        content                 => 'Package: *
Pin: release o=osbpo
Pin-Priority: 900
',
        notify                  => Exec['apt-update-after-osbpo'],
      }
      file { '/etc/oci/osbpo.asc':
        ensure                  => present,
        owner                   => 'root',
        group                   => 'root',
        mode                    => '0644',
        selinux_ignore_defaults => true,
        content                 => '-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF3QL7wBEAC0HwatykCC6Yldv2Pn+GRIed/qtzYJNIUjapD+U96nO1SW/RCs
5/SoJ87BWj/HjjwH9VW+WzCxBQDbXtzDpAIuZMovALZ7Q/XvgrQGk+3oIlVzjDK1
x8CH+Ers/aTFCZPBQyCcWOl4btombnpmOgyY5fMBz4X2mtxnxNqyF2zjVxZ8pgFP
tGpDaF5RhQasdOHgfx5VEVYCAk0IHxuAgkFd93GDvg5TY6/BgHnEnQR0iORCBTaM
AQHY3cr5M7r4DYIKl+Sk4B8C++jVIY9L3pyeJmIhSzToweNNNPJTExLNIBBDZWEq
Qv/WCZRtudxI8G95bdCiaUSm+ieJIMGhMFM5NKXincNzS7Pm7xD/rgANPYtuxrhR
Qd8S8NXD/fGWQwJB9o8jjE6TPwrX3zymyHAECxiY33Vwmgghyg27KwQ1dFCxBlH6
tRqjIly1lZVPd4EDJkuKbowN59b2230aLhAwi7r+DXUn9DSHRm/8s/gagy1XewW3
ZHA2aWZITrkGnXGOJRtbUhwcrwaLUnRx7zm4T12Mtky7jHphNXGTBaMW4/EnAtEj
MlKoECuNUjJml7km2pzOHRoNF+UpLK2bytflRl++qeDG2BEEC3SIZzIy2qPB68wl
cjdAPE1rRHJKZ4WsBoHfemXgy86YbWMRCvdL7W/uTlNJnzABAQO1Gbv36wARAQAB
tClBdXRvZ2VuZXJhdGVkIGtleSA8cm9vdEBvc2Jwby5kZWJpYW4ubmV0PokCTgQT
AQgAOBYhBA+46N0ZSY/skLJeXlYFarL+5O7LBQJd0C+8AhsvBQsJCAcDBRUKCQgL
BRYCAwEAAh4BAheAAAoJEFYFarL+5O7LcP0P/1oEbCtuMjO3nraOpUoYaS81hHwa
FSpAOoOMaR0a1ZRl3T83nDlYsPEU9oWpd8HiRbJelc3ZjXODU9XzmgXdhuGK3vZd
jJmYKmSJ/Dr7d+PCTO1ZmvOgVXyYJcJlIw+6WHg+/dtFC6uhiJKSYS71BudjFqLB
mcJ4vwe2ttRF5HG3XiUlfi5bVDYt/9vANA+h966leiXE0zFx89eWuGl8kxhJ8YI1
yv2WybskbTQAnOzWij3qeoF9OhKeXjWANalu0PTgal5zFL1Ar9n+JQlZVRXnYOx8
u4FVyt4NvUy4T7RhQeL8UsXBlVwH694+L9Y7T8p0jV4vZ7UvCR5zY+uGFSo6b7V1
DDn6uIaHaFt0DdndjQxhgP2kHyNB3f9tQrbVYxNR6XpsA5qHZVysSOqDGFE2V47H
4EiH0cyMJkAHQzGgPJa9DiMgNKI1luRkBgVsr79BbgoeOlKqpJ3bI2ViiTO/xDEr
mNVraGOCBYMTXYzR5xIQU9ob3ksD0ZX7jaeRdJpucbnWfHs0iZF/uC1H9Xl/LXbi
EMPRlhLlBsafsAb3xfiv8Uo7ULJ3Mc7bPiRtUO5nx1kS30m3yJlGv8dAYISn84h3
ts8N/5toE4NMsxqp47LX059LLaI2Fo2J7B+mhaYSVrgr2YNPRUu/62amB0AAZ44O
QxSClHlCzZqFu0KT
=kjjV
-----END PGP PUBLIC KEY BLOCK-----',
        notify                  => Exec['apt-update-after-osbpo'],
      }
    }else{
      file { '/etc/apt/preferences.d/99-prio-from-osbpo':
        ensure => absent,
        notify => Exec['apt-update-after-osbpo'],
      }
      file { '/etc/oci/osbpo.asc':
        ensure => absent,
        notify => Exec['apt-update-after-osbpo'],
      }
    }

    # ceph-reef repo from osbpo
    if $ceph_configure_osbpo {
      file { '/etc/apt/sources.list.d/ceph_reef.sources':
        ensure                  => present,
        owner                   => 'root',
        group                   => 'root',
        mode                    => '0644',
        selinux_ignore_defaults => true,
        content                 => 'Types: deb deb-src
Uris: http://osbpo.debian.net/debian
Suites: ceph-reef
Components: main
Architectures: amd64 arm64
Signed-By: /etc/oci/osbpo.asc
',
        notify                  => Exec['apt-update-after-osbpo'],
      }

    }else{
      file { '/etc/apt/sources.list.d/ceph_reef.sources':
        ensure => absent,
        notify => Exec['apt-update-after-osbpo'],
      }
    }

    # QEMU from official Debian backports
    if $nova_use_qemu_backports {
      file { '/etc/apt/sources.list.d/qemu.sources':
        ensure                  => present,
        owner                   => 'root',
        group                   => 'root',
        mode                    => '0644',
        selinux_ignore_defaults => true,
        content                 => 'Types: deb deb-src
Uris: http://osbpo.debian.net/debian
Suites: bookworm-qemu
Components: main
Architectures: amd64 arm64
Signed-By: /etc/oci/osbpo.asc
',
        notify                  => Exec['apt-update-after-osbpo'],
      }
    }else{
      file { '/etc/apt/sources.list.d/qemu.sources':
        ensure => absent,
        notify => Exec['apt-update-after-osbpo'],
      }
    }

    if $nova_use_libvirt_backports {
      file { '/etc/apt/sources.list.d/libvirt.sources':
        ensure                  => present,
        owner                   => 'root',
        group                   => 'root',
        mode                    => '0644',
        selinux_ignore_defaults => true,
        content                 => 'Types: deb deb-src
Uris: http://osbpo.debian.net/debian
Suites: bookworm-libvirt
Components: main
Architectures: amd64 arm64
Signed-By: /etc/oci/osbpo.asc
',
        notify                  => Exec['apt-update-after-osbpo'],
      }
    }else{
      file { '/etc/apt/sources.list.d/libvirt.sources':
        ensure => absent,
        notify => Exec['apt-update-after-osbpo'],
      }
    }

    # apt-update if repo files where created.
    exec { "apt-update-after-osbpo":
      command     => "/bin/true # comment to satisfy puppet syntax requirements
apt-get update
",
      logoutput   => 'on_failure',
      tries       => 3,
      try_sleep   => 1,
      refreshonly => true,
    }
  }
}
