# Install and configure a Ceph radosgw node for OCI

class oci::radosgw(
  $region_name              = 'RegionOne',
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $vip_ipaddr               = undef,
  $vip_hostname             = undef,
  $network_ipaddr           = undef,
  $network_cidr             = undef,
  $use_ssl                  = true,

  $ceph_admin_key           = undef,
  $ceph_bootstrap_rgw_key   = undef,
  $ceph_fsid                = undef,
  $ceph_mon_initial_members = undef,
  $ceph_mon_host            = undef,
  $pass_radosgw_authtoken   = undef,

  $use_ceph_cluster_net     = false,
  $cephnet_ip               = undef,
  $cephnet_network_addr     = undef,
  $cephnet_network_cidr     = undef,
  $cephnet_mtu              = undef,
  $self_signed_api_cert     = true,

  $external_keystone_activate              = false,
  $external_keystone_url                   = 'https://api.example.com/identity',
  $external_keystone_admin_password        = undef,
  $external_keystone_region_prefixed_users = false,

  # used by oci::swiftproxy_haproxy
  $swiftproxy_hostname        = 'none',
  $pass_haproxy_stats         = undef,
  $no_api_rate_limit_networks = [],
  $all_radosgw                = undef,
  $all_radosgw_ip             = undef,

  # From variables.json
  $kernel_from_backports          = false,
  $haproxy_timeout_http_request   = '10s',
  $haproxy_timeout_queue          = '1m',
  $haproxy_timeout_connect        = '10s',
  $haproxy_timeout_client         = '1m',
  $haproxy_timeout_server         = '1m',
  $haproxy_timeout_check          = '10s',
  $swift_proxy_haproxy_rate_limit = 80,
  $swift_proxy_disable_rate_limit = false,
){

  # Generic performances tweak (will default to throughput-performance)
  class { '::oci::tuned': profile => 'network-latency' }

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  if $use_ssl {
    $proto = 'https'
    $api_port = 443
  } else {
    $proto = 'http'
    $api_port = 80
  }

  if $use_ceph_cluster_net {
    $cluster_network = "${cephnet_network_addr}/${cephnet_network_cidr}"
    $cluster_ip = $cephnet_ip
  }else{
    $cluster_network = undef
    $cluster_ip = ""
  }

  class { 'ceph':
    fsid                => $ceph_fsid,
    ensure              => 'present',
    authentication_type => 'cephx',
    mon_initial_members => $ceph_mon_initial_members,
    mon_host            => $ceph_mon_host,
    public_network      => "${network_ipaddr}/${network_cidr}",
    cluster_network     => $cluster_network,
  }->
  ceph::key { 'client.admin':
    secret  => $ceph_admin_key,
    cap_mon => 'allow *',
    cap_osd => 'allow *',
    cap_mgr => 'allow *',
    cap_mds => 'allow',
  }
  ceph::key { "client.radosgw.${machine_hostname}":
    secret   => $ceph_bootstrap_rgw_key,
    #mode     => '0440',
    cap_mon  => 'allow rw',
    cap_osd  => 'allow rwx',
    inject  => true,
  }
  if $swiftproxy_hostname == "none" {
    $rgw_dns_name = $vip_hostname
  } else {
    $rgw_dns_name = $swiftproxy_hostname
  }
  ceph::rgw { "radosgw.${machine_hostname}":
    user               => 'ceph',
    rgw_dns_name       => $rgw_dns_name,
    rgw_swift_url      => "$proto://${rgw_dns_name}",
  }

  $base_url = "${proto}://${vip_hostname}"
  if $external_keystone_activate {
    $keystone_auth_uri  = $external_keystone_url
  }else{
    $keystone_auth_uri  = "${base_url}:${api_port}/identity"
  }
  if $external_keystone_region_prefixed_users {
    $radosgw_user          = "radosgw-${region_name}"
  } else {
    $radosgw_user          = 'radosgw'
  }

  ceph::rgw::keystone { "radosgw.${machine_hostname}":
    rgw_keystone_admin_domain   => 'default',
    rgw_keystone_admin_project  => 'services',
    rgw_keystone_admin_user     => $radosgw_user,
    rgw_keystone_admin_password => $pass_radosgw_authtoken,
    rgw_keystone_url            => "${keystone_auth_uri}",
    rgw_keystone_accepted_roles => 'member, admin, SwiftOperator',
  }
  ceph_config {
    "client.radosgw.${machine_hostname}/rgw_swift_account_in_url": value => true;
    "client.radosgw.${machine_hostname}/rgw_swift_versioning_enabled": value => true;
  }

  if $swiftproxy_hostname == "none" {
    debug("OCI will *NOT* setup a swiftproxy direct access.")
  } else {
    debug("OCI now setting-up a swiftproxy direct access.")
    $net_whitelist = concat($no_api_rate_limit_networks, $vip_ipaddr)
    class { 'oci::swiftproxy_haproxy':
      swiftproxy_hostname            => $swiftproxy_hostname,
      use_ssl                        => $use_ssl,
      haproxy_timeout_http_request   => $haproxy_timeout_http_request,
      haproxy_timeout_queue          => $haproxy_timeout_queue,
      haproxy_timeout_connect        => $haproxy_timeout_connect,
      haproxy_timeout_client         => $haproxy_timeout_client,
      haproxy_timeout_server         => $haproxy_timeout_server,
      haproxy_timeout_check          => $haproxy_timeout_check,
      machine_ip                     => $machine_ip,
      machine_hostname               => $machine_hostname,
      pass_haproxy_stats             => $pass_haproxy_stats,
      swift_proxy_haproxy_rate_limit => $swift_proxy_haproxy_rate_limit,
      swift_proxy_disable_rate_limit => $swift_proxy_disable_rate_limit,
      api_port                       => $api_port,
      net_whitelist                  => $net_whitelist,
      all_swiftproxy                 => $all_radosgw,
      all_swiftproxy_ip              => $all_radosgw_ip,
      backend_server_type            => 'radosgw',
    }
  }
}
