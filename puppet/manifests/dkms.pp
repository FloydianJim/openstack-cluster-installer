class oci::dkms(
){
  if $::lsbdistcodename == undef{
    # This works around differences between facter versions
    if $facts['os']['lsb'] != undef{
      $lsbdist = $facts['os']['lsb']['distcodename']
    }else{
      $lsbdist = $facts['os']['distro']['codename']
    }
  }else{
    $lsbdist = downcase($::lsbdistcodename)
  }
  case $lsbdist {
    'buster': {
      $kbuild_pkg_name = 'linux-kbuild-4.19'
    }
    'bullseye': {
      $kbuild_pkg_name = 'linux-kbuild-5.10'
    }
    'bookworm': {
      $kbuild_pkg_name = 'linux-kbuild-6.1'
    }
  }
  package { $kbuild_pkg_name:
    ensure => present,
  }
  package { 'dkms':
    ensure => present,
  }
  package { 'linux-headers-amd64':
    ensure => present,
  }
  file_line { 'mok_signing_key':
    path    => '/etc/dkms/framework.conf',
    match   => '.*mok_signing_key.*=.*',
    line    => 'mok_signing_key=/var/lib/shim-signed/mok/MOK.priv',
    require => Package['dkms'],
  }
  file_line { 'mok_certificate':
    path    => '/etc/dkms/framework.conf',
    match   => '.*mok_certificate.*=.*',
    line    => 'mok_certificate=/var/lib/shim-signed/mok/MOK.der',
    require => Package['dkms'],
  }
  file_line { 'mok_sign_tool':
    path    => '/etc/dkms/framework.conf',
    match   => '.*sign_tool.*=.*',
    line    => 'sign_tool=/etc/dkms/sign_helper.sh',
    require => Package['dkms'],
  }
  file { '/etc/dkms/sign_helper.sh':
    ensure                  => present,
    owner                   => 'root',
    content                 => '#!/bin/sh

/lib/modules/"$1"/build/scripts/sign-file sha512 /root/.mok/client.priv /root/.mok/client.der "$2"
',
    selinux_ignore_defaults => true,
    mode                    => '0750',
    require => Package['dkms'],
  }
}
