# Install and configure a CEPH OSD node for OCI
#
# == Parameters
#
# [*bootstrap_osd_key*]
# example: 'AQABsWZSgEDmJhAAkAGSOOAJwrMHrM5Pz5On1A=='
#
# [*fsid*]
# example: '066F558C-6789-4A93-AAF1-5AF1BA01A3AD'
#
# [*ceph_mon_initial_members*]
# example: 'mon1,mon2,mon3'
#
# [*ceph_mon_host*]
# example: '<ip of mon1>,<ip of mon2>,<ip of mon3>'
#
class oci::cephosd(
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $all_masters              = undef,
  $all_masters_ip           = undef,
  $vip_hostname             = undef,
  $vip_ipaddr               = undef,
  $vip_netmask              = undef,
  $network_ipaddr           = undef,
  $network_cidr             = undef,
  $use_ssl                  = true,

  $ceph_osd_initial_setup   = false,
  $block_devices            = undef,

  $ceph_admin_key           = undef,
  $ceph_openstack_key       = undef,
  $ceph_mon_key             = undef,
  $ceph_bootstrap_osd_key   = undef,
  $ceph_fsid                = undef,
  $ceph_mon_initial_members = undef,
  $ceph_mon_host            = undef,

  $use_ceph_cluster_net     = false,
  $cephnet_ip               = undef,
  $cephnet_network_addr     = undef,
  $cephnet_network_cidr     = undef,
  $cephnet_mtu              = undef,
  $self_signed_api_cert     = true,

  $bgp_to_the_host          = false,

  $pass_cephadm_ssh_pub     = undef,
  $pass_cephadm_ssh_priv    = undef,

  # From variables.json
  $kernel_from_backports    = false,

  $ceph_multiple_cluster_az = false,
  $ceph_az                  = 1,

  $ceph_use_cephadm_to_deploy = false,

  $configure_dkms = false,

  $ceph_configure_osbpo = false,
){

  # Configure ceph BPO
  class { 'oci::configure_backports':
    ceph_configure_osbpo       => $ceph_configure_osbpo,
  }

  # Generic performances tweak (will default to throughput-performance)
   class { '::oci::tuned': profile => 'network-latency' }

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  if $use_ceph_cluster_net {
    $cluster_network = "${cephnet_network_addr}/${cephnet_network_cidr}"
    $cluster_ip = $cephnet_ip
  }else{
    $cluster_network = undef
    $cluster_ip = ""
  }

  if $ceph_use_cephadm_to_deploy {
    package{'cephadm':
      ensure => present,
    }
    if $ceph_multiple_cluster_az {
      $key_name = "ceph-adm-key-az-${ceph_az}"
    }else{
      $key_name = 'ceph-adm-key'
    }
    ssh_authorized_key { $key_name:
      user => 'root',
      type => 'ssh-rsa',
      key  => $pass_cephadm_ssh_pub,
    }
  }else{
    class { '::ceph':
      fsid                => $ceph_fsid,
      ensure              => 'present',
      authentication_type => 'cephx',
      mon_initial_members => $ceph_mon_initial_members,
      mon_host            => $ceph_mon_host,
      public_network      => "${network_ipaddr}/${network_cidr}",
      cluster_network     => $cluster_network,
    }->
    ::ceph::key { 'client.admin':
      secret  => $ceph_admin_key,
      cap_mon => 'allow *',
      cap_osd => 'allow *',
      cap_mds => 'allow',
    }->
    ::ceph::key { 'client.openstack':
      secret  => $ceph_openstack_key,
      mode    => '0644',
      cap_mon => 'profile rbd',
      cap_osd => 'profile rbd pool=cinder, profile rbd pool=nova, profile rbd pool=glance, profile rbd pool=gnocchi, profile rbd pool=cinderback',
    }->
    ::ceph::key { 'client.bootstrap-osd':
      secret       => $ceph_bootstrap_osd_key,
      keyring_path => '/var/lib/ceph/bootstrap-osd/ceph.keyring',
      cap_mon      => 'allow profile bootstrap-osd',
    }->
    class { '::ceph::profile::params':
      fsid                => $ceph_fsid,
      manage_repo         => false,
      release             => 'nautilus',
      authentication_type => 'cephx',
      mon_host            => $ceph_mon_host,
      mon_initial_members => $ceph_mon_initial_members,
      client_keys         => {
        'client.admin' => {
          secret  => $ceph_admin_key,
          cap_mon => 'allow *',
          cap_osd => 'allow *',
          cap_mds => 'allow',
        },
        'client.openstack' => {
          secret  => $ceph_openstack_key,
          mode    => '0644',
          cap_mon => 'profile rbd',
          cap_osd => 'profile rbd pool=cinder, profile rbd pool=nova, profile rbd pool=glance, profile rbd pool=gnocchi, profile rbd pool=cinderback',
        },
        'client.bootstrap-osd' => {
          secret       => $ceph_bootstrap_osd_key,
          keyring_path => '/var/lib/ceph/bootstrap-osd/ceph.keyring',
          cap_mon      => 'allow profile bootstrap-osd',
        }
      }
    }

    # Appending some lines in an ugly manner instead
    ['[osd]','debug_osd = 0','debug_bluestore = 0','debug_rocksdb = 0','debug_bluefs = 0','osd_max_backfills = 1','osd_recovery_max_active = 1','osd_memory_target = 4G','osd_pg_log_trim_min = 10','osd_pg_log_dups_tracked = 10','osd_min_pg_log_entries = 10','osd_max_pg_log_entries = 10','osd_recovery_sleep=0.1','osd_recovery_sleep_ssd=0.1'].each |$line| {
      file_line { "ensure $line in /etc/ceph/ceph.conf":
        path  => '/etc/ceph/ceph.conf',
        line  => "$line",
        match => '$line',
      }
    }
  }
}
