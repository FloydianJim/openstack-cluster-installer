#
# Manages /etc/systemd/system/puppet.service.d/oci-ca-cert.conf
# 
class oci::puppet_oci_ca_cert(
  $self_signed_api_cert     = true,
){
  file { "/etc/systemd/system/puppet.service.d":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }

  $str = "[Service]
Environment=OS_CACERT=/etc/ssl/certs/oci-pki-oci-ca-chain.pem
"

  if $self_signed_api_cert {
    file { "/etc/systemd/system/puppet.service.d/oci-ca-cert.conf":
      ensure                  => present,
      owner                   => 'root',
      mode                    => '0644',
      content                 => $str,
      selinux_ignore_defaults => true,
    }
    # This one influences the way puppet is ran
    # from oci-puppet, and is often forgotten
    # when switching from self-signed to a real
    # SSL certificate.
    file { "/etc/oci/self-signed-api-cert":
      ensure                  => directory,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
    }
  }else{
    file { "/etc/systemd/system/puppet.service.d/oci-ca-cert.conf":
      ensure                  => absent,
      owner                   => 'root',
      mode                    => '0644',
      selinux_ignore_defaults => true,
    }
    file { "/etc/oci/self-signed-api-cert":
      ensure                  => absent,
      selinux_ignore_defaults => true,
    }
  }
}
