class oci::swiftstore(
  $region_name               = 'RegionOne',
  $machine_hostname          = undef,
  $machine_ip                = undef,
  $network_ipaddr            = undef,
  $network_cidr              = undef,
  $block_devices             = undef,
  $use_oci_sort_dev          = false,

  $statsd_hostname           = undef,
  $pass_swift_hashpathsuffix = undef,
  $pass_swift_hashpathprefix = undef,
  $zoneid                    = undef,
  $use_ssl                   = true,
  $all_swiftstore_ip         = undef,
  $all_swiftstore            = undef,
  $all_swiftproxy            = undef,
  $all_swiftproxy_ip         = undef,
  $monitoring_host           = undef,
  $monitoring_graphite_host  = undef,
  $monitoring_graphite_port  = undef,
  $swift_object_replicator_concurrency = 5,
  $swift_rsync_connection_limit        = 25,
  $self_signed_api_cert      = true,

  $swift_store_account       = true,
  $swift_store_container     = true,
  $swift_store_object        = true,

  $servers_per_port           = 1,
  $network_chunk_size         = 65535,
  $disk_chunk_size            = 524288,

  $swift_storage_allowed_networks = undef,

  ### Comming from variables.json ###
  # EC policy
  $swift_ec_enable               = false,
  $swift_ec_policy_index         = 1,
  $swift_ec_policy_name          = 'pyeclib-12-4',
  $swift_ec_type                 = 'liberasurecode_rs_vand',
  $swift_ec_num_data_fragments   = 12,
  $swift_ec_num_parity_fragments = 4,
  $swift_ec_object_segment_size  = 1048576,

  # Rsync QoS limits
  $swift_rsync_io_limit_activate  = false,
  $swift_rsync_io_limit_megabytes = 80,
  $swift_rsync_io_limit_iops      = 100,

  # Reserved storage space management
  $swiftstore_reserved_space             = 300,
  $swiftstore_fallocate_reserved_space   = 200,
  $swift_override_cluster_reserved_space = false,
  $swift_storage_reserved_space          = 500,
  $swift_storage_fallocate_reserved_space = 300,

  $swift_auto_mount_block_devices = true,

  $kernel_from_backports = false,
){
  if $swift_override_cluster_reserved_space {
    $storage_reserved_space = $swift_storage_reserved_space
  }else{
    $storage_reserved_space = $swiftstore_reserved_space
  }
  if $swift_override_cluster_reserved_space {
    $fallocate_reserve = $swift_storage_fallocate_reserved_space * 1024 * 1024 * 1024
  }else{
    $fallocate_reserve = $swiftstore_fallocate_reserved_space * 1024 * 1024 * 1024
  }

  # Generic performances tweak (will default to throughput-performance)
  # class { '::oci::tuned': }

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  class { '::oci::swiftcommon':
    swift_store_account   => $swift_store_account,
    swift_store_container => $swift_store_container,
    swift_store_object    => $swift_store_object,
  }

  # Install memcache
  class { '::memcached':
    listen_ip => '127.0.0.1',
    udp_port  => 0,
    max_memory => '20%',
  }

  # Fireall object, account and container servers,
  # so that only our management network has access to it.
  # First, general definirion (could be in global site.pp)
  resources { "firewall":
    purge   => true
  }
  class { 'firewall': }

  firewall { '010 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }->
  firewall { '011 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }->
  firewall { '012 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }->
  firewall { "056 Allow SMTP on 127.0.0.1 TCP":
    proto       => tcp,
    action      => accept,
    destination => "127.0.0.1",
    dport       => '25',
  }->
  firewall { "057 Allow SMTP from ${machine_ip} to ${machine_ip} TCP":
    proto       => tcp,
    action      => accept,
    source      => "${machine_ip}",
    destination => "${machine_ip}",
    dport       => '25',
  }->
  firewall { '058 Jump to LOGDROP for SMTP':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '25',
  }->
  firewall { '059 Jump to LOGDROP for RPCBIND':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '111',
  }->
  firewall { '060 Jump to LOGDROP for RPCBIND UDP':
    proto       => udp,
    jump        => 'LOGDROP',
    dport       => '111',
  }->
  firewall { '061 Jump to LOGDROP for BGP':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '179',
  }

  $all_allowed_ips = concat($all_swiftproxy_ip, $all_swiftstore_ip)

  $swift_storage_allowed_networks.each |Integer $index, String $value| {
    $val1 = $index*3+100
    $val2 = $index*3+101
    $val3 = $index*3+102
    firewall { "${val1} Allow ${value} to access to swift container and account servers":
      proto       => tcp,
      action      => accept,
      source      => "${value}",
      dport       => '6001-6002',
    }->
    firewall { "${val2} Allow ${value} to access to swift object servers":
      proto       => tcp,
      action      => accept,
      source      => "${value}",
      dport       => '6200-6229',
    }->
    firewall { "${val3} Allow ${value} to access rsync":
      proto       => tcp,
      action      => accept,
      source      => "${value}",
      dport       => '873',
    }
  }

  firewall { '801 Jump to LOGDROP for container and account servers':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '6001-6002',
  }

  firewall { '802 Jump to LOGDROP for object servers':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '6200-6229',
  }

  firewall { '803 Jump to LOGDROP for rsync':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '873',
  }

  firewall { '804 Jump to LOGDROP for SMTP':
    proto       => tcp,
    jump        => 'LOGDROP',
    dport       => '25',
  }

  firewallchain { 'LOGDROP:filter:IPv4':
    ensure  => present,
  }

  firewall { '901 LOG rule for dropped packets':
    chain       => 'LOGDROP',
    proto       => tcp,
    jump        => 'LOG',
    log_level   => '6',
    log_prefix  => 'swift dropped packet',
    limit       => '1/sec',
  }
  firewall { "902 Deny all access to container and account server":
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '6001-6002',
  }
  firewall { "903 Deny all access to object server":
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '6200-6229',
  }
  firewall { '904 Deny all access to rsync':
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '873',
  }
  firewall { '905 Deny all access to SMTP':
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '25',
  }
  firewall { '906 Deny all access to RPCBIND':
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '111',
  }
  firewall { '907 Deny all access to RPCBIND UDP':
    chain       => 'LOGDROP',
    proto       => udp,
    action      => reject,
    dport       => '111',
  }
  firewall { '908 Deny all access to BGP':
    chain       => 'LOGDROP',
    proto       => tcp,
    action      => drop,
    dport       => '179',
  }

  class { 'swift':
    swift_hash_path_suffix => $pass_swift_hashpathsuffix,
    swift_hash_path_prefix => $pass_swift_hashpathprefix,
  }

  if $swift_rsync_io_limit_activate {
    package { 'cgroup-tools':
      ensure => present,
    }
    ::systemd::dropin_file {'io-limit-slice.conf':
      unit    => 'rsync.service',
      content => '[Service]
Slice=rsync.slice
IOSchedulingClass=best-effort
IOSchedulingPriority=7',
      notify  => Service['rsync'],
    }->
    ::systemd::unit_file {'rsync.slice':
      content => "[Slice]
IOAccounting=1
IOWriteBandwidthMax=/srv/node/sda ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdb ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdc ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdd ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sde ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdf ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdg ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdh ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdi ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdj ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdk ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdl ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdm ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdn ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdo ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdp ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdq ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdr ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sds ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdt ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdu ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdv ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdw ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdx ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdy ${swift_rsync_io_limit_megabytes}M
IOWriteBandwidthMax=/srv/node/sdz ${swift_rsync_io_limit_megabytes}M
IOWriteIOPSMax=/srv/node/sda ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdb ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdc ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdd ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sde ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdf ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdg ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdh ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdi ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdj ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdk ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdl ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdm ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdn ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdo ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdp ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdq ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdr ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sds ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdt ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdu ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdv ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdw ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdx ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdy ${swift_rsync_io_limit_iops}
IOWriteIOPSMax=/srv/node/sdz ${swift_rsync_io_limit_iops}
",
      notify  => Service['rsync'],
    }
    file {'/etc/xinetd.d/rsync':
      ensure => absent,
      notify => Service['rsync'],
    }->
    exec {'reload-xinetd':
      command => '/usr/bin/systemctl restart xinetd.service',
      onlyif  => "/usr/bin/ss -ltne | grep ${machine_ip}:873 | grep -q xinetd.service",
      notify  => Service['rsync'],
    }->
    class { '::swift::storage':
      storage_local_net_ip   => $machine_ip,
      rsync_use_xinetd       => false,
      require                => Systemd::Unit_file['rsync.slice'],
      use_drive_full_checker => true
    }
  }else{
    file {'/etc/systemd/system/rsync.slice':
      ensure => absent,
      notify => Service['rsync'],
    }->
    file {'/etc/systemd/system/rsync.service.d/io-limit-slice.conf':
      ensure => absent,
      notify => Service['rsync'],
    }->
    file {'/etc/xinetd.d/rsync':
      ensure => absent,
      notify => Service['rsync'],
    }->
    exec {'reload-xinetd':
      command => '/usr/bin/systemctl restart xinetd.service',
      onlyif  => "/usr/bin/ss -ltne | grep ${machine_ip}:873 | grep -q xinetd.service",
    }->
    class { '::swift::storage':
      storage_local_net_ip => $machine_ip,
      rsync_use_xinetd     => false,
      use_drive_full_checker => true
    }
  }
  class { 'swift::storage::drive_full_checker':
    user                      => 'root',
    account_max_connections   => $swift_rsync_connection_limit,
    account_reserved_space    => $storage_reserved_space,
    container_max_connections => $swift_rsync_connection_limit,
    container_reserved_space  => $storage_reserved_space,
    object_max_connections    => $swift_rsync_connection_limit,
    object_reserved_space     => $storage_reserved_space,
  }

  if $swift_store_container {
    include swift::storage::container
  }
  if $swift_store_account {
    include swift::storage::account
  }
  if $swift_store_object {
    include swift::storage::object
  }

  if $statsd_hostname == ''{
    $statsd_enabled = false
  } else {
    $statsd_enabled = true
  }

  if $statsd_hostname == '' and $monitoring_graphite_host == ''{
    notice("Please defile statsd_hostname and monitoring_graphite_host to enable Collectd")
  }else{
    # Collected
    class { '::collectd':
      purge           => true,
      interval        => 60,
      recurse         => true,
      purge_config    => true,
      minimum_version => '5.4',
    }
    class { 'collectd::plugin::statsd':
      host            => '127.0.0.1',
      port            => 8125,
      timersum        => 'true',
      timercount      => 'true',
    }
    collectd::plugin::write_graphite::carbon {"${monitoring_graphite_host}-carbon":
      graphitehost   => $monitoring_graphite_host,
      graphiteport   => 2003,
      protocol       => 'tcp',
      storerates     => false
    }
  }

  if $swift_store_account {
    swift::storage::server { '6002':
      type                     => 'account',
      devices                  => '/srv/node',
      rsync_module_per_device  => true,
      device_names             => $block_devices,
      config_file_path         => 'account-server.conf',
      storage_local_net_ip     => "${machine_ip}",
      pipeline                 => ['healthcheck', 'recon', 'account-server'],
      max_connections          => $swift_rsync_connection_limit,
      statsd_enabled           => $statsd_enabled,
      log_statsd_host          => $statsd_hostname,
      log_statsd_metric_prefix => $machine_hostname,
      fallocate_reserve        => $fallocate_reserve,
      server_fallocate_reserve => $fallocate_reserve,
      log_name_per_daemon      => true,
    }
    swift_account_uwsgi_config {
      'uwsgi/disable-logging': value => true;
    }
  }else{
    package { 'swift-account':
      ensure => absent,
    }
  }

  if $swift_store_container {
    swift::storage::server { '6001':
      type                     => 'container',
      devices                  => '/srv/node',
      rsync_module_per_device  => true,
      device_names             => $block_devices,
      config_file_path         => 'container-server.conf',
      storage_local_net_ip     => "${machine_ip}",
      pipeline                 => ['healthcheck', 'recon', 'container-server'],
      max_connections          => $swift_rsync_connection_limit,
      replicator_node_timeout  => 120,
      statsd_enabled           => $statsd_enabled,
      log_statsd_host          => $statsd_hostname,
      log_statsd_metric_prefix => $machine_hostname,
      fallocate_reserve        => $fallocate_reserve,
      server_fallocate_reserve => $fallocate_reserve,
      log_name_per_daemon      => true,
    }
    swift_container_uwsgi_config {
      'uwsgi/disable-logging': value => true;
    }
  }else{
    package { 'swift-container':
      ensure => absent,
    }
  }

  if $swift_store_object {
    swift::storage::server { '6200':
      type                      => 'object',
      devices                   => '/srv/node',
      rsync_module_per_device   => true,
      device_names              => $block_devices,
      config_file_path          => 'object-server.conf',
      storage_local_net_ip      => "${machine_ip}",
      pipeline                  => ['healthcheck', 'recon', 'object-server'],
      max_connections           => $swift_rsync_connection_limit,
      servers_per_port          => $servers_per_port,
      replicator_concurrency    => $swift_object_replicator_concurrency,
      statsd_enabled            => $statsd_enabled,
      log_statsd_host           => $statsd_hostname,
      log_statsd_metric_prefix  => $machine_hostname,
      network_chunk_size        => $network_chunk_size,
      disk_chunk_size           => $disk_chunk_size,
      object_server_mb_per_sync => 16,
      client_timeout            => 345,
      fallocate_reserve         => $fallocate_reserve,
      server_fallocate_reserve  => $fallocate_reserve,
      log_name_per_daemon       => true,
    }
  }else{
    package { 'swift-object':
      ensure => absent,
    }
  }

  # With this, we use the OCI's udev rule to order drives
  if $use_oci_sort_dev {
    $base_dir = '/dev/disk/oci-sort'
  }else{
    $base_dir = '/dev'
  }

  if $swift_auto_mount_block_devices {
    $block_devices.each |Integer $index, String $value| {
      swift::storage::disk { "${value}":
        base_dir          => $base_dir,
        mount_type        => 'uuid',
        require           => Class['swift'],
        manage_partition  => false,
        manage_filesystem => false,
      }
    }
  }

  if $swift_ec_enable {
    swift::storage::policy { $swift_ec_policy_index:
      policy_name             => $swift_ec_policy_name,
      default_policy          => false,
      policy_type             => 'erasure_coding',
      ec_type                 => $swift_ec_type,
      ec_num_data_fragments   => $swift_ec_num_data_fragments,
      ec_num_parity_fragments => $swift_ec_num_parity_fragments,
      ec_object_segment_size  => $swift_ec_object_segment_size,
    }
  }

  if $swift_store_account {
    $rings1 = [ 'account' ]
  }else{
    $rings1 = [ ]
  }
  if $swift_store_object {
    $rings2 = concat($rings1, ['object'])
  }else{
    $rings2 = $rings1
  }
  if $swift_store_container {
    $rings3 = concat($rings2, ['container'])
  }else{
    $rings3 = $rings2
  }
  swift::storage::filter::recon { $rings3: }
  swift::storage::filter::healthcheck { $rings3: }

  Swift::Ringsync<<||>>
}
