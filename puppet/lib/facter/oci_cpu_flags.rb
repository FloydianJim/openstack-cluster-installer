require 'open3'
Facter.add(:oci_cpu_flags) do
  setcode do
    mytable = []
    cmd = "lscpu | grep Flags | sed s/Flags://"
    output, exit_code = Open3.capture2(cmd)
    output.split.map().each do |val|
      mytable << val
    end
    mytable
  end
end
