#!/bin/sh

set -e
#set -x

if ! [ -r /etc/oci-poc/oci-poc.conf ] ; then
	echo "Cannot read /etc/oci-poc/oci-poc.conf"
fi
. /etc/oci-poc/oci-poc.conf

# Check that we really have NUMBER_OF_GUESTS machines available
# before starting anything
check_enough_vms_available () {
	EXPECTED_NUM_OF_SLAVES=${1}

	NUM_VM=$(ocicli -csv machine-list | q -d , -H "SELECT COUNT(*) AS count FROM -")
	if [ ${NUM_VM} -lt ${EXPECTED_NUM_OF_SLAVES} ] ; then
		echo "Num of VM too low... exiting"
		exit 1
	fi
}

check_enough_vms_available $((${NUMBER_OF_GUESTS} - 1))

echo "===> Setting-up IPMI ports on VMs"
ocicli machine-set-ipmi C1 yes 192.168.100.1 9002 ipmiusr test
ocicli machine-set-ipmi C2 yes 192.168.100.1 9003 ipmiusr test
ocicli machine-set-ipmi C3 yes 192.168.100.1 9004 ipmiusr test
ocicli machine-set-ipmi C4 yes 192.168.100.1 9005 ipmiusr test
ocicli machine-set-ipmi C5 yes 192.168.100.1 9006 ipmiusr test
ocicli machine-set-ipmi C6 yes 192.168.100.1 9007 ipmiusr test
ocicli machine-set-ipmi C7 yes 192.168.100.1 9008 ipmiusr test
ocicli machine-set-ipmi C8 yes 192.168.100.1 9009 ipmiusr test
ocicli machine-set-ipmi C9 yes 192.168.100.1 9010 ipmiusr test
ocicli machine-set-ipmi CA yes 192.168.100.1 9011 ipmiusr test
ocicli machine-set-ipmi CB yes 192.168.100.1 9012 ipmiusr test
ocicli machine-set-ipmi CC yes 192.168.100.1 9013 ipmiusr test
ocicli machine-set-ipmi CD yes 192.168.100.1 9014 ipmiusr test
ocicli machine-set-ipmi CE yes 192.168.100.1 9015 ipmiusr test
ocicli machine-set-ipmi CF yes 192.168.100.1 9016 ipmiusr test
ocicli machine-set-ipmi D0 yes 192.168.100.1 9017 ipmiusr test
ocicli machine-set-ipmi D1 yes 192.168.100.1 9018 ipmiusr test
ocicli machine-set-ipmi D2 yes 192.168.100.1 9019 ipmiusr test
ocicli machine-set-ipmi D3 yes 192.168.100.1 9020 ipmiusr test
ocicli machine-set-ipmi D4 yes 192.168.100.1 9021 ipmiusr test
ocicli machine-set-ipmi D5 yes 192.168.100.1 9022 ipmiusr test
ocicli machine-set-ipmi D6 yes 192.168.100.1 9023 ipmiusr test
ocicli machine-set-ipmi D7 yes 192.168.100.1 9024 ipmiusr test
ocicli machine-set-ipmi D8 yes 192.168.100.1 9025 ipmiusr test
ocicli machine-set-ipmi D9 yes 192.168.100.1 9026 ipmiusr test
ocicli machine-set-ipmi DA yes 192.168.100.1 9027 ipmiusr test
ocicli machine-set-ipmi DB yes 192.168.100.1 9028 ipmiusr test
ocicli machine-set-ipmi DC yes 192.168.100.1 9029 ipmiusr test
ocicli machine-set-ipmi DD yes 192.168.100.1 9030 ipmiusr test

echo "===> Creating regions and locations"
ocicli swift-region-create swift-region1
ocicli swift-region-create swift-region2
ocicli swift-region-create pub-region

ocicli location-create reg-1-zone-1 swift-region2
ocicli location-create reg-2-zone-1 swift-region1
ocicli location-create reg-2-zone-2 swift-region1
ocicli location-create pub-zone pub-region

echo "===> Creating networks"
ocicli network-create reg1-zone1-net1 192.168.101.0 24 reg-1-zone-1 no
ocicli network-create reg2-zone1-net1 192.168.103.0 24 reg-2-zone-1 no
ocicli network-create reg2-zone2-net1 192.168.104.0 24 reg-2-zone-2 no
ocicli network-create pub-net 192.168.106.0 24 pub-zone yes

# Set the IPMI network
if [ "${USE_AUTOMATIC_IPMI_SETUP}" = "yes" ] ; then
	echo "===> Setting-up automatic IPMI assignation"
	ocicli network-create ipmi 192.168.200.0 24 reg-1-zone-1 no
	ocicli network-set ipmi --role ipmi --ipmi-match-addr 192.168.0.0 --ipmi-match-cidr 16
	ssh ${HOST_NETWORK_PREFIX}.2 "sed -i s/automatic_ipmi_numbering=no/automatic_ipmi_numbering=yes/ /etc/openstack-cluster-installer/openstack-cluster-installer.conf" 1>/dev/null 2>/dev/null
	ssh ${HOST_NETWORK_PREFIX}.2 "mkdir -p /var/www/.ssh" 1>/dev/null 2>/dev/null
	ssh ${HOST_NETWORK_PREFIX}.2 "chown www-data:www-data /var/www/.ssh" 1>/dev/null 2>/dev/null
fi

echo "===> Creating cluster cl1"
ocicli cluster-create cl1 infomaniak.ch

echo "===> Adding networks to cl1"
ocicli network-add reg1-zone1-net1 cl1 all eth1 none
ocicli network-add reg2-zone1-net1 cl1 all eth1 none
ocicli network-add reg2-zone2-net1 cl1 all eth3 none
ocicli network-add pub-net cl1 all eth1 none
ocicli network-set pub-net --role vip

echo "===> Adding controller nodes to cl1"
# 3x Controller machines (includes Swift proxies)
ocicli machine-add C1 cl1 controller reg-1-zone-1
ocicli machine-add C2 cl1 controller reg-2-zone-1
ocicli machine-add C3 cl1 controller reg-2-zone-2

echo "===> Adding network nodes to cl1"
# 2x Network
ocicli machine-add C4 cl1 network reg-1-zone-1
ocicli machine-add C5 cl1 network reg-2-zone-1

echo "===> Adding swiftproxy nodes to cl1"
# 1x Swift proxy
ocicli machine-add C6 cl1 swiftproxy reg-1-zone-1

echo "===> Adding cephmon nodes to cl1"
# 3x Ceph Mon
ocicli machine-add C7 cl1 cephmon reg-1-zone-1
ocicli machine-add C8 cl1 cephmon reg-2-zone-1
ocicli machine-add C9 cl1 cephmon reg-2-zone-2

echo "===> Adding compute nodes to cl1"
# 2x Compute (with Ceph OSD hyperconverged)
ocicli machine-add CA cl1 compute reg-1-zone-1
ocicli machine-add CB cl1 compute reg-2-zone-1

echo "===> Addig Ceph OSD feature to compute nodes"
ocicli machine-set CA --compute-is-cephosd yes
ocicli machine-set CB --compute-is-cephosd yes

echo "===> Adding cephosd node"
ocicli machine-add CC cl1 cephosd reg-2-zone-2

echo "===> Adding swiftstore nodes to cl1"
# 5x Swift Store
ocicli machine-add CD cl1 swiftstore reg-1-zone-1
ocicli machine-add CE cl1 swiftstore reg-2-zone-1
ocicli machine-add CF cl1 swiftstore reg-2-zone-1
ocicli machine-add D0 cl1 swiftstore reg-2-zone-2
ocicli machine-add D1 cl1 swiftstore reg-2-zone-2

echo "===> Adding volume nodes to cl1"
# 2x Volume
ocicli machine-add D2 cl1 volume reg-1-zone-1
ocicli machine-add D3 cl1 volume reg-2-zone-1

echo "===> Adding messaging nodes to cl1"
# 3x Messaging
ocicli machine-add D4 cl1 messaging reg-1-zone-1
ocicli machine-add D5 cl1 messaging reg-2-zone-1
ocicli machine-add D6 cl1 messaging reg-2-zone-1

echo "===> Adding a tempest node to cl1"
# 1x tempest
ocicli machine-add D7 cl1 tempest reg-2-zone-1

echo "===> Adding 3 billmon node to cl1"
ocicli machine-add D8 cl1 billmon reg-1-zone-1
ocicli machine-add D9 cl1 billmon reg-1-zone-1
ocicli machine-add DA cl1 billmon reg-1-zone-1

echo "===> Adding 3 billosd node to cl1"
ocicli machine-add DB cl1 billosd reg-1-zone-1
ocicli machine-add DC cl1 billosd reg-1-zone-1
ocicli machine-add DD cl1 billosd reg-1-zone-1


echo "===> Calculating swift ring"
# Calculate ring
ocicli swift-calculate-ring cl1 100 100 100

exit 0
