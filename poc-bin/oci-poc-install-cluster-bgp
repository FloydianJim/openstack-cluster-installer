#!/bin/sh

set -e
#set -x

if ! [ -r /etc/oci-poc/oci-poc.conf ] ; then
	echo "Cannot read /etc/oci-poc/oci-poc.conf"
fi
. /etc/oci-poc/oci-poc.conf

# Check that we really have NUMBER_OF_GUESTS machines available
# before starting anything
check_enough_vms_available () {
	EXPECTED_NUM_OF_SLAVES=${1}

	NUM_VM=$(ocicli -csv machine-list | q -d , -H "SELECT COUNT(*) AS count FROM -")
	if [ ${NUM_VM} -lt ${EXPECTED_NUM_OF_SLAVES} ] ; then
		echo "Num of VM too low... exiting"
		exit 1
	fi
}

check_enough_vms_available $((${NUMBER_OF_GUESTS} - 1))

echo "===> Setting-up IPMI ports on VMs"
for i in $(seq 2 $((${NUMBER_OF_GUESTS} + 1))) ; do
	VM_SERIAL=$(printf "%X\n" $((0xBF + ${i})))
	VNC_PORT=$((9000 + $i))
	echo "Setting IPMI for VM with serial: $VM_SERIAL and VNC port: $VNC_PORT"
	ocicli machine-set-ipmi ${VM_SERIAL} yes 192.168.200.1 ${VNC_PORT} ipmiusr test
done

echo "===> Creating regions and locations"
ocicli swift-region-create swift-region1
ocicli swift-region-create swift-region2
ocicli swift-region-create swift-region3
ocicli swift-region-create l2
ocicli swift-region-create pub-region

ocicli location-create reg-1-zone-1 swift-region1
ocicli location-create reg-2-zone-1 swift-region2
ocicli location-create reg-3-zone-1 swift-region3
ocicli location-create reg-l2-zone-1 l2
ocicli location-create pub-zone pub-region

echo "===> Creating networks"
ocicli network-create reg1-zone1-net1 192.168.101.0 26 reg-1-zone-1 no
ocicli network-create reg2-zone1-net1 192.168.101.64 26 reg-2-zone-1 no
ocicli network-create reg3-zone1-net1 192.168.101.128 26 reg-3-zone-1 no
# We use the L2 used for booting-up servesr in rack 3...
ocicli network-create reg-l2 192.168.113.0 24 reg-l2-zone-1 no
# ...though to avoid conclifts, we use the last IPs of the range.
ocicli network-set reg-l2 --ip 192.168.113.0 --first-ip 192.168.113.200 --last-ip 192.168.113.220 --vlan 11 --iface1 1g1 --iface2 none

#ocicli network-create br-lb 0.0.0.0 24 reg-1-zone-1 no
ocicli network-create pub-net 192.168.106.1 32 pub-zone yes

# Set the IPMI network
if [ "${USE_AUTOMATIC_IPMI_SETUP}" = "yes" ] ; then
	echo "===> Setting-up automatic IPMI assignation"
	ocicli network-create ipmi 192.168.200.0 24 reg-1-zone-1 no
	ocicli network-set ipmi --role ipmi --ipmi-match-addr 192.168.0.0 --ipmi-match-cidr 16
	ssh ${HOST_NETWORK_PREFIX}.2 "sed -i s/automatic_ipmi_numbering=no/automatic_ipmi_numbering=yes/ /etc/openstack-cluster-installer/openstack-cluster-installer.conf" 1>/dev/null 2>/dev/null
	ssh ${HOST_NETWORK_PREFIX}.2 "mkdir -p /var/www/.ssh" 1>/dev/null 2>/dev/null
	ssh ${HOST_NETWORK_PREFIX}.2 "chown www-data:www-data /var/www/.ssh" 1>/dev/null 2>/dev/null
fi

echo "===> Creating cluster cl1"
ocicli cluster-create cl1 infomaniak.ch

echo "===> Setting BGP-to-host in cl1"
ocicli cluster-set cl1 --bgp-to-the-host yes --asn 65499 --neutron-use-dvr no --region-name cluster1
# This is necessary, otherwise we may have TCP connectivity issues.
# In real life, we could setup MTU to 9050 everywhere in our ikvswitch setups,
# but it currently doesn't work well enough, and it's not so important, as long
# as the internal OpenStack connectivity works.
ocicli cluster-set cl1 --neutron-global-physnet-mtu 1450 --neutron-path-mtu 1500

echo "===> Setting-up messaging VIP"
ocicli network-create messaging-vip 192.168.106.2 32 pub-zone no
ocicli network-add messaging-vip cl1 vip 1g1 none
ocicli network-set messaging-vip --role vip --vip-usage messaging

echo "===> Adding networks to cl1"
ocicli network-add reg1-zone1-net1 cl1 all 1g1 1g2
ocicli network-add reg2-zone1-net1 cl1 all 1g1 1g2
ocicli network-add reg3-zone1-net1 cl1 all 1g1 1g2
ocicli network-add reg-l2 cl1 all 1g1 none
ocicli network-add pub-net cl1 all ens5 none
ocicli network-set pub-net --role vip
ocicli network-set reg1-zone1-net1 --vlan 10

echo "===> Adding controller nodes to cl1"
# 3x Controller machines (includes Swift proxies)
ocicli machine-add C1 cl1 controller reg-1-zone-1
ocicli machine-add C2 cl1 controller reg-2-zone-1
ocicli machine-add C3 cl1 controller reg-3-zone-1

echo "===> Adding compute nodes to cl1"
# 2x Compute (with Ceph OSD hyperconverged)
ocicli machine-add C4 cl1 compute reg-1-zone-1
ocicli machine-add C5 cl1 compute reg-2-zone-1

ocicli machine-add E1 cl1 compute reg-1-zone-1
ocicli machine-add E2 cl1 compute reg-2-zone-1
ocicli machine-add E3 cl1 compute reg-3-zone-1
ocicli machine-add E4 cl1 compute reg-1-zone-1
ocicli machine-add E5 cl1 compute reg-2-zone-1
ocicli machine-add E6 cl1 compute reg-3-zone-1

echo "===> Setting compute nodes to use Ceph and nova-reserved-host-memory-mb to 8192"
ocicli machine-set C4 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set C5 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192

ocicli machine-set E1 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set E2 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set E3 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set E1 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set E2 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set E3 --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192

#echo "===> Addig Ceph OSD feature to compute nodes"
#ocicli machine-set C4 --compute-is-cephosd yes
#ocicli machine-set C5 --compute-is-cephosd yes

echo "===> Adding network nodes to cl1"
# 2x Network
ocicli machine-add C6 cl1 network reg-l2-zone-1
ocicli machine-add C7 cl1 network reg-l2-zone-1

echo "===> Disabling BGP-to-host on network nodes, and adding DRAgent"
ocicli machine-set C6 --force-no-bgp2host yes --neutron-install-dragent yes
ocicli machine-set C7 --force-no-bgp2host yes --neutron-install-dragent yes

echo "===> Adding swiftproxy nodes to cl1"
# 1x Swift proxy
ocicli machine-add C8 cl1 swiftproxy reg-2-zone-1

echo "===> Adding a tempest node to cl1"
# 1x tempest
ocicli machine-add C9 cl1 tempest reg-3-zone-1

echo "===> Adding DNS nodes"
ocicli machine-add CA cl1 dns reg-2-zone-1
ocicli machine-add CB cl1 dns reg-3-zone-1


echo "===> Adding cephmon nodes to cl1"
# 3x Ceph Mon
ocicli machine-add CC cl1 cephmon reg-1-zone-1
ocicli machine-add CD cl1 cephmon reg-2-zone-1
ocicli machine-add CE cl1 cephmon reg-3-zone-1

echo "===> Adding swiftstore nodes to cl1"
# 5x Swift Store
ocicli machine-add CF cl1 swiftstore reg-1-zone-1
ocicli machine-add D0 cl1 swiftstore reg-2-zone-1
ocicli machine-add D1 cl1 swiftstore reg-3-zone-1

echo "===> Setting reserved space limit for swift"
ocicli cluster-set cl1 --swiftstore-fallocate-reserved-space 5 --swiftstore-reserved-space 10 --swiftproxy-fallocate-reserved-space 5 --swiftproxy-reserved-space 10

echo "===> Adding volume nodes to cl1"
# 2x Volume
ocicli machine-add D2 cl1 volume reg-1-zone-1
ocicli machine-add D3 cl1 volume reg-2-zone-1
ocicli machine-add D4 cl1 volume reg-3-zone-1

echo "===> Adding messaging nodes to cl1"
# 3x Messaging
ocicli machine-add D5 cl1 messaging reg-1-zone-1
ocicli machine-add D6 cl1 messaging reg-2-zone-1
ocicli machine-add D7 cl1 messaging reg-3-zone-1

echo "===> Adding billmon nodes"
ocicli machine-add D8 cl1 billmon reg-1-zone-1
ocicli machine-add D9 cl1 billmon reg-2-zone-1
ocicli machine-add DA cl1 billmon reg-3-zone-1

echo "===> Adding billosd nodes"
ocicli machine-add DB cl1 billosd reg-1-zone-1
ocicli machine-add DC cl1 billosd reg-2-zone-1
ocicli machine-add DD cl1 billosd reg-3-zone-1

echo "===> Adding cephosd nodes"
ocicli machine-add DE cl1 cephosd reg-1-zone-1
ocicli machine-add DF cl1 cephosd reg-2-zone-1
ocicli machine-add E0 cl1 cephosd reg-3-zone-1

echo "===> Creating the SQL VIP"
ocicli network-create cl1-vpi-sql 192.168.106.7 32 pub-zone
ocicli network-add cl1-vpi-sql cl1 all 1g1 none
ocicli network-set cl1-vpi-sql --role vip --vip-usage sql

echo "===> Creating the SQL MSG VIP"
ocicli network-create cl1-vpi-sql-msg 192.168.106.8 32 pub-zone
ocicli network-add cl1-vpi-sql-msg cl1 all 1g1 none
ocicli network-set cl1-vpi-sql-msg --role vip --vip-usage sqlmsg

echo "===> Adding SQL nodes"
ocicli machine-add F0 cl1 sql reg-1-zone-1
ocicli machine-add F1 cl1 sql reg-2-zone-1
ocicli machine-add F2 cl1 sql reg-3-zone-1

echo "===> Adding SQL MSG nodes"
ocicli machine-add F3 cl1 sqlmsg reg-1-zone-1
ocicli machine-add F4 cl1 sqlmsg reg-2-zone-1
ocicli machine-add F5 cl1 sqlmsg reg-3-zone-1

echo "===> Reduce the (default) number of fragment in swift"
ocicli cluster-set cl1 --swift-ec-num-data-fragments 6 --swift-ec-num-parity-fragments 2 --swift-ec-enable no --install-designate yes --disable-notifications yes --glance-prefer-ceph-over-swift no

echo "===> Setting-up cl1-swiftproxy-1.infomaniak.ch as swift-proxy-hostname"
ocicli cluster-set cl1 --swift-proxy-hostname cl1-swiftproxy-1.infomaniak.ch --swift-public-cloud-middlewares yes --cinder-default-volume-type CEPH_1

if [ "${USE_IKVSWITCH}" = yes ] ; then
	echo "===> Setting-up BGP and VLAN"
	ocicli cluster-set cl1 --bgp-to-the-host yes
	ocicli network-set reg1-zone1-net1 --vlan ${BGP_TO_HOST_VLAN}
	ocicli network-set reg2-zone1-net1 --vlan ${BGP_TO_HOST_VLAN}
	ocicli network-set reg3-zone1-net1 --vlan ${BGP_TO_HOST_VLAN}
fi

echo "===> Calculating swift ring"
# Calculate ring
ocicli swift-calculate-ring cl1 100 100 100

echo "===> Creating Keystone cluster cl2"
ocicli cluster-create cl2 infomaniak.ch
# We create the 2nd cluster with region-name cluster1, to avoid having
# 3 regions from the customer point of view, even though we really have
# 3 regions...
ocicli cluster-set cl2 --bgp-to-the-host yes --asn 65499 --neutron-use-dvr no --install-barbican no --install-heat no --install-rabbit no --region-name cluster1 --disable-notifications yes

echo "===> Creating cl2 network"
ocicli swift-region-create swift-region-cl2
ocicli location-create location-cl2 swift-region-cl2
ocicli network-create cl2-net1 192.168.121.0 26 location-cl2 no
ocicli network-add cl2-net1 cl2 all 1g1 1g2
ocicli network-set cl2-net1 --vlan 10

echo "===> Creating cl2 VIP"
ocicli swift-region-create cl2-vip-swift-region
ocicli location-create cl2-vip-location cl2-vip-swift-region
ocicli network-create cl2-vip-net 192.168.106.10 32 pub-zone yes
ocicli network-add cl2-vip-net cl2 all ens5 none
ocicli network-set cl2-vip-net --role vip --vlan 10
ocicli cluster-set cl2 --bgp-to-the-host yes

echo "===> Adding Keystone nodes to cl2"
ocicli machine-add E8 cl2 controller location-cl2
ocicli machine-add E9 cl2 controller location-cl2
ocicli machine-add EB cl2 controller location-cl2

echo "===> Creating Compute cluster cl3"
ocicli cluster-create cl3 infomaniak.ch
ocicli cluster-set cl3 --bgp-to-the-host yes --asn 65499 --neutron-use-dvr no --region-name cluster3
# Same reason as above.
ocicli cluster-set cl3 --neutron-global-physnet-mtu 1450 --neutron-path-mtu 1500

echo "===> Creating cl3 network"
ocicli swift-region-create swift-region-cl3
ocicli location-create location-cl3 swift-region-cl3
ocicli network-create cl3-net1 192.168.122.0 26 location-cl3 no
ocicli network-add cl3-net1 cl3 all 1g1 1g2
ocicli network-set cl3-net1 --vlan 10

echo "===> Creating l2 connected controller networks"
ocicli swift-region-create l2-cl3
ocicli location-create l2-cl3 l2-cl3
ocicli network-create l2-ctrls 192.168.118.0 24 l2-cl3 no
ocicli network-set l2-ctrls --ip 192.168.118.0 --first-ip 192.168.118.200 --last-ip 192.168.118.220 --vlan 16 --iface1 1g1 --iface2 none
ocicli network-add l2-ctrls cl3 all 1g1 none

echo "===> Creating cl3 VIP"
ocicli swift-region-create cl3-vip-swift-region
ocicli location-create cl3-vip-location cl3-vip-swift-region
ocicli network-create cl3-vip-net 192.168.106.12 32 pub-zone yes
ocicli network-add cl3-vip-net cl3 all ens5 none
ocicli network-set cl3-vip-net --role vip --vlan 10
ocicli cluster-set cl3 --bgp-to-the-host yes

echo "===> Adding Controller nodes to cl3"
ocicli machine-add E7 cl3 controller l2-cl3
ocicli machine-add EA cl3 controller l2-cl3
ocicli machine-add ED cl3 controller l2-cl3
ocicli machine-set E7 --force-no-bgp2host yes --force-frr yes
ocicli machine-set EA --force-no-bgp2host yes --force-frr yes
ocicli machine-set ED --force-no-bgp2host yes --force-frr yes

echo "===> Adding Compute nodes to cl3"
ocicli machine-add EC cl3 compute location-cl3
ocicli machine-add EE cl3 compute location-cl3
ocicli machine-add EF cl3 compute location-cl3
ocicli machine-set EC --compute-is-cephosd yes --ceph-osd-initial-setup yes --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set EE --compute-is-cephosd yes --ceph-osd-initial-setup yes --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192
ocicli machine-set EF --compute-is-cephosd yes --ceph-osd-initial-setup yes --use_ceph_if_available yes --nova-reserved-host-memory-mb 8192

exit 0
